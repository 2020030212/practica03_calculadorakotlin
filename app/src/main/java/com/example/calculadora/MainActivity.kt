package com.example.calculadora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    private lateinit var btnIngresar : Button;
    private lateinit var btnSalir : Button;
    private lateinit var txtUsuario : EditText;
    private  lateinit var txtContraseña : EditText;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes();
        btnIngresar.setOnClickListener{ ingresar() }
        btnSalir.setOnClickListener {
            salir()
        }
    }

    private fun iniciarComponentes(){

            btnIngresar = findViewById(R.id.btnIngresar)
            btnSalir = findViewById(R.id.btnSalir)
            txtUsuario = findViewById(R.id.txtUsuario)
            txtContraseña = findViewById(R.id.txtContraseña)
    }

        private fun ingresar(){
            var strUsuario : String
            var strContraseña : String


            strUsuario = applicationContext.resources.getString(R.string.usuario)
            strContraseña = applicationContext.resources.getString(R.string.contraseña)


            if(strUsuario.toString().equals(txtUsuario.text.toString()) && strContraseña.toString().equals(txtContraseña.text.toString())){

                // HACER EL PAQUETE PARA MANDAR LA INFORMACIÓN
                var bundle = Bundle();
                bundle.putString("usuario", txtUsuario.text.toString())

                // HACER INTENT PARA LLAMAR OTRA ACTIVIDAD
                val intent = Intent(this@MainActivity, CalculadoraActivity2::class.java)
                intent.putExtras(bundle)

                // INICIAR LA ACTIVIDAD ESPERANDO O NO RESPUESTA
                startActivity(intent)


            } else{
                Toast.makeText(this.applicationContext, "El usuario o contraseña no es valido", Toast.LENGTH_LONG).show()
            }
        }

        private fun salir(){
            var confirmar = AlertDialog.Builder(this)
            confirmar.setTitle("Calculadora")
            confirmar.setMessage("Deseas salir")
            confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
            confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
            confirmar.show()
        }
    }
