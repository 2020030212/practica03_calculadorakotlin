package com.example.calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

private lateinit var btnSumar : Button
private lateinit var  btnRestar : Button
private lateinit var  btnMultiplicar : Button
private lateinit var btnDividir : Button
private lateinit var btnLimpiar : Button
private lateinit var btnRegresar : Button
private lateinit var lblUsuario : TextView
private lateinit var lblResultado : TextView
private lateinit var  txtUno : EditText
private lateinit var txtDos : EditText

//declarar el objeto
private var calculadora = Calculadora(0, 0)

class CalculadoraActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora2)
        iniciarComponentes();

        //obtener los datos del mainactivity
        var datos = intent.extras
        var usuario = datos!!.getString("usuario")
        lblUsuario.text = usuario.toString()


        btnSumar.setOnClickListener { btnSumar() }
        btnRestar.setOnClickListener{ btnResta() }
        btnMultiplicar.setOnClickListener {btnMultiplicacion() }
        btnDividir.setOnClickListener{btnDivision() }

        btnLimpiar.setOnClickListener{btnLimpiar() }
        btnRegresar.setOnClickListener{ btnRegresar()}
    }

    private fun iniciarComponentes(){
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        lblUsuario = findViewById(R.id.lblUsuario)
        lblResultado = findViewById(R.id.lblResultado)

        txtUno = findViewById(R.id.txtNum1)
        txtDos = findViewById(R.id.txtNum2)

    }

    fun btnSumar(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this.applicationContext, "Ingrese los numeros correspondientes", Toast.LENGTH_LONG).show()
        } else{
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.suma();
            lblResultado.text = total.toString()
        }

    }

    fun btnResta(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this.applicationContext, "Ingrese los numeros correspondientes", Toast.LENGTH_LONG).show()
        } else {
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.resta();
            lblResultado.text = total.toString()
        }
    }

    fun btnMultiplicacion(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this.applicationContext, "Ingrese los numeros correspondientes", Toast.LENGTH_LONG).show()
        } else {
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.multiplicacion();
            lblResultado.text = total.toString()
        }
    }

    fun btnDivision(){
        if(txtUno.text.toString().isEmpty() || txtDos.text.toString().isEmpty()){
            Toast.makeText(this.applicationContext, "Ingrese los numeros correspondientes", Toast.LENGTH_LONG).show()
        } else {
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.division();
            lblResultado.text = total.toString()
        }
    }

    fun btnLimpiar(){
        lblResultado.setText("")
        txtUno.setText("")
        txtDos.setText("")
    }

    fun btnRegresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Regresar al MainActivity")
        confirmar.setPositiveButton("Confirmar"){dialogInterface, which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface, which->}
        confirmar.show()
    }

}